#!/usr/bin/env sh

set -euo pipefail

find . -name \*.go  -exec goimports -w {} \;

