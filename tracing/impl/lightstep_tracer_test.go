// +build tracer_static,tracer_static_lightstep

package impl

import (
	"testing"

	"gitlab.com/gitlab-org/labkit/tracing/connstr"
)

func Test_lightstepTracerFactory(t *testing.T) {
	tests := []struct {
		connectionString string
		wantErr          bool
		strict           bool
	}{
		{
			connectionString: "opentracing://lightstep",
			wantErr:          true,
			strict:           true,
		},
		{
			connectionString: "opentracing://lightstep?access_token=12345",
			wantErr:          false,
			strict:           true,
		},
		{
			connectionString: "opentracing://lightstep?access_token=12345&relaxed",
			wantErr:          false,
			strict:           false,
		},
		{
			connectionString: "opentracing://lightstep?access_token=12345&strict",
			wantErr:          true,
			strict:           true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.connectionString, func(t *testing.T) {
			_, options, err := connstr.Parse(tt.connectionString)
			if err != nil {
				t.Errorf("TracerFactory() error = unable to parse connection string: %v", err)
			}
			if tt.strict {
				options[keyStrictConnectionParsing] = "1"
			}

			options["service_name"] = "test"

			gotTracer, gotCloser, err := lightstepTracerFactory(options)

			if (err != nil) != tt.wantErr {
				t.Errorf("TracerFactory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !tt.wantErr {
				if gotTracer == nil {
					t.Errorf("TracerFactory() expected a tracer, got nil")
				}
				if gotCloser == nil {
					t.Errorf("TracerFactory() expected a closed, got nil")
				}
			}
		})
	}
}
