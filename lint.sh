#!/usr/bin/env sh

set -euo pipefail
# Write the code coverage report to gl-code-quality-report.json
# and print linting issues to stdout in the format: path/to/file:line description
# https://docs.gitlab.com/ee/development/go_guide/#automatic-linting
golangci-lint run --out-format code-climate | tee gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
