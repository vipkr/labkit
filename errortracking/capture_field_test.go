package errortracking

import (
	"testing"

	"github.com/getsentry/sentry-go"
	"github.com/stretchr/testify/require"
)

func TestWithField(t *testing.T) {
	event := sentry.NewEvent()
	domain := "http://example.com"
	config := &captureConfig{}

	WithField("domain", domain)(config, event)

	require.True(t, event.Tags["domain"] == domain)
}
