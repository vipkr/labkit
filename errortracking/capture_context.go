package errortracking

import (
	"context"

	"github.com/getsentry/sentry-go"
	"gitlab.com/gitlab-org/labkit/correlation"
)

const sentryExtraKey = "gitlab.CorrelationID"

// WithContext will extract information from the context to add to the error.
func WithContext(ctx context.Context) CaptureOption {
	return func(config *captureConfig, event *sentry.Event) {
		correlationID := correlation.ExtractFromContext(ctx)
		if correlationID != "" {
			event.Tags[sentryExtraKey] = correlationID
		}
	}
}
