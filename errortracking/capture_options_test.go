package errortracking

import (
	"testing"

	"github.com/getsentry/sentry-go"
	"github.com/stretchr/testify/require"
)

func Test_applyCaptureOptions(t *testing.T) {
	tests := []struct {
		name           string
		opts           []CaptureOption
		wantConfig     captureConfig
		wantEventLevel sentry.Level
	}{
		{
			name:           "default",
			opts:           []CaptureOption{},
			wantConfig:     captureConfig{},
			wantEventLevel: sentry.LevelError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotConfig, gotEvent := applyCaptureOptions(tt.opts)
			gotEventLevel := gotEvent.Level

			require.Equalf(t, gotConfig, tt.wantConfig, "applyCaptureOptions() = %v, want %v", gotConfig, tt.wantConfig)
			require.Equalf(t, gotEventLevel, tt.wantEventLevel, "applyCaptureOptions() Event.Level = %v, want %v", gotEventLevel, tt.wantEventLevel)
		})
	}
}
